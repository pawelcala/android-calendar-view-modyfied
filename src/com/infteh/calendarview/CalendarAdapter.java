package com.infteh.calendarview;

import java.util.Calendar;
import java.util.List;
import java.util.Random;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

/**
 * calendar adapter.
 * @author Sazonov-adm
 * 
 */
public class CalendarAdapter extends BaseAdapter {
	
	private static int STAT_ID = 0;
	
	private int id;

	/**
	 * references to our items.
	 */
	private DayCell[] days;

	/**
	 * context.
	 */
	private Context mContext;

	/**
	 * init month.
	 */
	private java.util.Calendar mCurrentMonth;
	/**
	 * today.
	 */
	private Calendar mToday;

	/**
	 * day that was picked.
	 */
	private Calendar mCurrentDay;
	
	private EventCheckAdapter adapter;

	/**
	 * @param context context.
	 * @param monthCalendar current month.
	 */
	public CalendarAdapter(Context context, Calendar monthCalendar) {
		
		id = STAT_ID;
		STAT_ID ++;
		
		mToday = DateHelper.createCurrentBeginDayCalendar();
		mCurrentMonth = (Calendar) monthCalendar.clone();
		mContext = context;
		mCurrentMonth.set(Calendar.DAY_OF_MONTH, 1);
		Log.i("ca", id + " con"+monthCalendar.get(Calendar.MONTH) );
		refreshDays();
	}

	/**
	 * @param currentDay day that was picked.
	 */
	public void setCurrentDay(Calendar currentDay) {
		mCurrentDay = currentDay;
	}
	
	public void setDayesWithEvent( EventCheckAdapter adapter ){
		this.adapter = adapter;
	}

	/**
	 * @param month current month.
	 */
	public final void setMonth(Calendar month) {
		mCurrentMonth = (Calendar) month.clone();
		Log.i("ca", id + " setMonth"+month.get(Calendar.MONTH) );
	}

	public int getCount() {
		return days.length;
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return 0;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View currentView = convertView;
		TextView dayViewTextView;
		DayCell dayCell = days[position];
		if (convertView == null) {
			LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			currentView = vi.inflate(R.layout.calendar_item, null);
		}
		
		if( mCurrentMonth.get(Calendar.MONTH) > 0 ){
			int i = 0;
		}
		
		
		
		Calendar realTime = Calendar.getInstance();
		dayViewTextView = (TextView) currentView.findViewById(R.id.date);
		int currentMonth = dayCell.mDate.get(Calendar.MONTH) - realTime.get(Calendar.MONTH);
		int currentDay = dayCell.mDate.get(Calendar.DAY_OF_YEAR) - realTime.get(Calendar.DAY_OF_YEAR);
		if ( currentMonth < 0 ) {
			dayViewTextView.setTextColor(mContext.getResources().getColor(R.color.calendar_past_font_color));
		} else if( currentMonth > 0 ){
			dayViewTextView.setTextColor(mContext.getResources().getColor(R.color.calendar_future_font_color));
		} else {
			dayViewTextView.setTextColor(mContext.getResources().getColor(R.color.calendar_font_color));
		}

		//Log.i("ca", id + " getView"+mCurrentMonth.get(Calendar.MONTH) +"/" + currentMonth +"/" + currentDay);
		
		
		//NOT USED!
//		if (DateHelper.isWeekendDay(dayCell.mDate)) {
//			if (currentMonth == 0 ) {
//				dayViewTextView.setTextColor(mContext.getResources().getColor(R.color.calendar_weekend_font_color));
//			} else if( currentMonth > 0 ){
//				
//			} else {
//				dayViewTextView.setTextColor(mContext.getResources().getColor(R.color.calendar_secondary_weekend_font_color));
//			}
//		}
		
		if( currentMonth < 0 ){
			currentView.setBackgroundResource(R.drawable.custom_future_selector);
			dayViewTextView.setTextColor(0xff969698);
			dayViewTextView.setShadowLayer(0, 0, 0, Color.TRANSPARENT);
			Log.i("ca", id + " daycell < /"+mCurrentMonth.get(Calendar.MONTH)+"/"+dayCell.getDate().get(Calendar.MONTH) + "/" + dayCell.getDate().get(Calendar.DAY_OF_MONTH));
		}else if( currentMonth > 0 ){
			currentView.setBackgroundResource(R.drawable.custom_future_selector);
			dayViewTextView.setTextColor(0xff969698);
			dayViewTextView.setShadowLayer(0, 0, 0, Color.TRANSPARENT);
			Log.i("ca", id + " daycell > /"+mCurrentMonth.get(Calendar.MONTH)+"/"+dayCell.getDate().get(Calendar.MONTH) + "/" + dayCell.getDate().get(Calendar.DAY_OF_MONTH));
		}else{
			if( currentDay < 0 ){
				Log.i("ca", id + " daycell d< /"+mCurrentMonth.get(Calendar.MONTH)+"/"+dayCell.getDate().get(Calendar.MONTH) + "/" + dayCell.getDate().get(Calendar.DAY_OF_MONTH));
				currentView.setBackgroundResource(R.drawable.custom_past_selector);
				dayViewTextView.setTextColor(0xff808080);
				dayViewTextView.setShadowLayer(2, 0, 2, Color.WHITE);
			}else if( currentDay > 0 ){
				Log.i("ca", id + " daycell d> /"+mCurrentMonth.get(Calendar.MONTH)+"/"+dayCell.getDate().get(Calendar.MONTH) + "/" + dayCell.getDate().get(Calendar.DAY_OF_MONTH));
				currentView.setBackgroundResource(R.drawable.custom_present_selector);
				dayViewTextView.setTextColor(0xff06184d);
				dayViewTextView.setShadowLayer(2, 0, 2, Color.WHITE);
			}else{
				Log.i("ca", id + " daycell d= /"+mCurrentMonth.get(Calendar.MONTH)+"/"+dayCell.getDate().get(Calendar.MONTH) + "/" + dayCell.getDate().get(Calendar.DAY_OF_MONTH));
				currentView.setBackgroundResource(R.drawable.custom_today_selector);
				dayViewTextView.setTextColor(mContext.getResources().getColor(android.R.color.white));
				dayViewTextView.setShadowLayer(2, 0, 2, Color.BLACK);
			}
		}
/*
		// choosing background
		if (mToday.equals(dayCell.mDate)) {
			currentView.setBackgroundResource(R.drawable.custom_today_selector);
			dayViewTextView.setTextColor(mContext.getResources().getColor(android.R.color.white));
			dayViewTextView.setShadowLayer(2, 0, 2, Color.BLACK);
			//currentView.setBackgroundResource(R.drawable.calendar_today_background);
		} else if (mCurrentDay != null && DateHelper.equalsIgnoreTime(mCurrentDay.getTime(), dayCell.mDate.getTime())) {
			//currentView.setBackgroundResource(R.drawable.calendar_item_background_current);
			currentView.setBackgroundResource(R.drawable.custom_today_selector);
			dayViewTextView.setTextColor(mContext.getResources().getColor(android.R.color.white));
			dayViewTextView.setShadowLayer(2, 0, 2, Color.BLACK);
		} else {
			dayViewTextView.setShadowLayer(2, 0, 2, Color.WHITE);
			if (currentMonth == 0 ) {
				currentView.setBackgroundResource(R.drawable.custom_present_selector);
			} else{
				currentView.setBackgroundResource(R.drawable.custom_future_selector);
			}
		}
		*/
		
		// check if with indicator
		
		if( dayCell.getIsWithExtra() ){
			currentView.findViewById(R.id.date_indicator).setVisibility(View.VISIBLE);
			currentView.findViewById(R.id.date_indicator ).setSelected( mToday.equals(dayCell.mDate ) );
		}else{
			currentView.findViewById(R.id.date_indicator).setVisibility(View.INVISIBLE);
		}
		
		dayViewTextView.setText(dayCell.getDescr());

		// create date string for comparison
		String date = dayCell.getDescr();

		if (date.length() == 1) {
			date = "0" + date;
		}

		String monthStr = "" + (mCurrentMonth.get(Calendar.MONTH) + 1);
		if (monthStr.length() == 1) {
			monthStr = "0" + monthStr;
		}
		
		currentView.setTag(dayCell);
		
		return currentView;
	}

	/**
	 * refresh.
	 */
	public final void refreshDays() {
		Calendar currentDate = DateHelper.fromDateToCalendar(DateHelper.clearTime(((Calendar) mCurrentMonth.clone()).getTime()));
		currentDate.set(Calendar.DAY_OF_MONTH, 1);

		int firstDay = currentDate.get(Calendar.DAY_OF_WEEK);
		int dayShift;
		if (DateHelper.isMondayFirst()) {
			if (firstDay == Calendar.SUNDAY) {
				dayShift = 6;
			} else {
				dayShift = firstDay - 2;
			}
		} else {
			dayShift = firstDay - 1;
		}
		currentDate.add(Calendar.DATE, -dayShift);
		days = new DayCell[42];
		for (int i = 0; i < days.length; i++) {
			days[i] = new DayCell(currentDate, "" + currentDate.get(Calendar.DAY_OF_MONTH));
			if( adapter != null && adapter.isDayWithEvent(days[i]) ){
				days[i].setIsWithExtra(true);
			}
			
			DateHelper.increment(currentDate);
		}
	}

	/**
	 * @author Sazonov-adm
	 * 
	 */
	public class DayCell {
		
		private boolean isWithExtra = false;
		
		/**
		 * descr.
		 */
		private String mDescription;
		/**
		 * date.
		 */
		private Calendar mDate;

		/**
		 * @param currentDate date.
		 * @param text descr.
		 */
		public DayCell(Calendar currentDate, String text) {
			mDescription = text;
			mDate = (Calendar) currentDate.clone();
		}
		
		public void setIsWithExtra( boolean flag ){
			this.isWithExtra = flag;
		}
		
		public boolean getIsWithExtra(){
			return isWithExtra;
		}

		/**
		 * @return descr.
		 */
		public final String getDescr() {
			return mDescription;
		}

		/**
		 * @return date.
		 */
		public final Calendar getDate() {
			return mDate;
		}

		@Override
		public final int hashCode() {
			return super.hashCode();
		}

		@Override
		public final boolean equals(Object other) {
			// Not strictly necessary, but often a good optimization
			if (this == other) {
				return true;
			}
			if (!(other instanceof DayCell)) {
				return false;
			}
			DayCell otherA = (DayCell) other;
			return (mDate.equals(otherA.mDate)) && ((mDate == null) ? otherA.mDate == null : mDate.equals(otherA.mDate));
		}
	}
}