package com.infteh.calendarview;

import java.util.Calendar;
import java.util.List;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * calendar view.
 * @author Sazonov-adm
 *
 */
public class CalendarView extends FrameLayout {
	
	private boolean smoothMonthSliding = true;
	
	/**
	 * pager.
	 */
	private ViewPager pager;
	
	/**
	 * adapter.
	 */
	private MonthPagerAdapter adapter;
	
	/**
	 * РљРѕРЅСЃС‚СЂСѓРєС‚РѕСЂ.
	 * 
	 * @param context РєРѕРЅС‚РµРєСЃС‚
	 */
	public CalendarView(final Context context) {
		this(context, null);
	}

	/**
	 * РљРѕРЅСЃС‚СЂСѓРєС‚РѕСЂ.
	 * 
	 * @param context РєРѕРЅС‚РµРєСЃС‚
	 * @param attrs Р°С‚СЂРёР±СѓС‚С‹
	 */
	public CalendarView(final Context context, final AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	
	
	private void init(){
		LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.calendar_view, this, true);
		pager = (ViewPager) this.findViewById(R.id.calendar_view_pager);
		
		//FIXME Вообще, искать сразу нельзя. Может быть null. Нужно искать в onFinishInflate.
		//pager = ((ViewPager) findViewById(R.id.calendar_view_pager));
		adapter = new MonthPagerAdapter(inflater, pager);
		pager.setAdapter(adapter);
		
		//pager.setCurrentItem(0);//MonthPagerAdapter.INFINITE / 2);
		//addView(pager);
	}
	
	/**
	 * Р—Р°СЂРµРіРёСЃС‚СЂРёСЂРѕРІР°С‚СЊ РЅРѕРІС‹Р№ РЅР°Р±Р»СЋРґР°С‚РµР»СЊ.
	 * 
	 * @param observer РќР°Р±Р»СЋРґР°С‚РµР»СЊ.
	 */
	public final void registerCalendarDatePickObserver(final CalendarDatePick observer) {
		((MonthPagerAdapter) pager.getAdapter()).setPickObserver(observer);
	}
	
	public void setDayesWithEvents( EventCheckAdapter checkAdapter ){
		if( adapter != null ){
			adapter.setDayesWithEvents(checkAdapter);
		}
	}
	
	public int getCalendarMonthHeight(){
		if( pager != null ){
			View v = pager.getChildAt(0);
			if( v != null ){
				return v.getMeasuredHeight();
			}
		}
		return -1;
	}
	
	public void setSmoothMonthSliding( boolean flag ){
		smoothMonthSliding = flag;
	}
	
	public void setOnPageChangeListener( OnPageChangeListener listener ){
		if( pager != null ){
			pager.setOnPageChangeListener(listener);
		}
	}
	
	public void setPagerHeight( int heght ){
		if( pager != null ){
			pager.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, heght) );
			this.invalidate();
			
		}
	}
	
	public void showNextMonth(){
		if( pager != null && pager.getCurrentItem() < pager.getChildCount() ){
			pager.setCurrentItem(pager.getCurrentItem()+1, smoothMonthSliding);
		}
	}
	
	public void showPreviousMonth(){
		if( pager != null && pager.getCurrentItem() > 0 ){
			pager.setCurrentItem(pager.getCurrentItem()-1, smoothMonthSliding);
		}
	}
	
	/**
	 * set current day.
	 * @param month month.
	 */
	public final void setCurrentDay(Calendar currentDay) {
		adapter.setCurrentDay(currentDay);
	}
	
	/**
	 * set current month.
	 * @param month month.
	 */
	public final void setMonth(Calendar month) {
		adapter.setMonth(month);
	}

	/**
	 * get current.
	 * @return month month.
	 */
	public final Calendar getMonth() {
		return adapter.getMonth();
	}
	
	public void setCalendarPage( int page ){
		if( pager != null && pager.getCurrentItem() != page && page < adapter.getCount() ){
			pager.setCurrentItem(page);
		}
	}
}
