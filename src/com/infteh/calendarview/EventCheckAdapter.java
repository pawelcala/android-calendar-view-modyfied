package com.infteh.calendarview;

import com.infteh.calendarview.CalendarAdapter.DayCell;

public interface EventCheckAdapter {
	public boolean isDayWithEvent( DayCell dayCell );	
}
