package com.infteh.calendarview;

import java.util.Calendar;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.GridView;

public class MyCalendarView extends FrameLayout{
	
	private GridView gridView;
	private CalendarAdapter adapter;

	public MyCalendarView(Context context) {
		super(context);
		init();
	}

	public MyCalendarView(Context context, AttributeSet attrs) {
		super(context, attrs );
		init();
	}

	
	public MyCalendarView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public void init(){
		final GridView view = (GridView) LayoutInflater.from(getContext()).inflate(R.layout.grid_template, null);
		CalendarAdapter adapter = new CalendarAdapter( getContext(), Calendar.getInstance() );
		view.setAdapter(adapter);
		addView(view);
	}
	
	public void setMonth( Calendar month ){
		adapter.setMonth(month);
	}
	
	public void setCurrentDay( Calendar currentDay ){
		adapter.setCurrentDay(currentDay );
	}
	
	
}
